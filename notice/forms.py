from django import forms
from .models import Notices

class NoticeCreateForm(forms.ModelForm):
    nfaculty = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter the faculty'}
    ), required=True, max_length=100)
    nbatch = forms.IntegerField(widget=forms.NumberInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter batch'}
    ), required=False)
    nsemester = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter  Semester'}
    ), required=False, max_length=50)
    nnotice = forms.CharField(widget=forms.Textarea(
        attrs={'class': 'form-control', 'placeholder': 'Enter  Semester'}
    ), required=False, max_length=50)
    ntitle = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter  Semester'}
    ), required=False, max_length=50)
    ntype = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter  Semester'}
    ), required=False, max_length=50)
    display = forms.DateField(widget=forms.DateInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter anniversary date', 'id': 'Anniversary'}
    ), required=False)

    class Meta:
        model = Notices
        fields = ['nfaculty', 'nbatch', 'nsemester', 'nnotice', 'ntitle', 'ntype','display']