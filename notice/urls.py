from django.urls import path
from .views import (
    notice,
    StudentNotice,
	)

app_name='notice'
urlpatterns = [
    path('notice/',notice,name='notice'),
    path('student-notice-list/',StudentNotice.as_view(),name='student-notice'),
]
