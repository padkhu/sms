from django.shortcuts import render,redirect
from django.views.generic import TemplateView
from django.views.generic import ListView

from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.http import HttpResponse


from .models import Notices
from .forms import NoticeCreateForm
from faculty.models import Faculty

# class Notice(TemplateView):

#     def get (self,request):
#         context = {}
#         return render(request, 'notice/notice.html',{})

# class NoticeCreate(CreateView):
#     model=Notices
#     template_name = 'notice/notice.html'
#     form_class = NoticeCreateForm
#     success_url = '/notice/'


def notice(request):
	if request.method == "POST":
		form = NoticeCreateForm(request.POST)
		print('faculty',form['nfaculty'].value())
		print('batch',form['nbatch'].value())
		print('semester',form['nsemester'].value())
		print('notice',form['nnotice'].value())
		print('title',form['ntitle'].value())
		print('type',form['ntype'].value())
		print('display',form['display'].value())
		if form.is_valid():
			try:
				form.save()
				return redirect('notice:notice')
			except:
				return HttpResponse('ouuyt')
		else:
			return HttpResponse('invalid form')
	notices = Notices.objects.all()
	faculty = Faculty.objects.all()
	form = NoticeCreateForm()
	return render(request, "notice/notice.html", {'form':form,'faculty':faculty,'notices':notices})


class StudentNotice(ListView):
    template_name = 'notice/notice-student-list.html'
    def get(self, request, *args, **kwargs):
        notices = Notices.objects.all()
        return render(request, self.template_name, {'notices':notices})
