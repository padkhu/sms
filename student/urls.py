from django.urls import path
from .views import (
    StudentDashboard,
    StudentProfile,
    StudentProfileUpdate,
	)

app_name='student'
urlpatterns = [
    path('student-dashboard/',StudentDashboard.as_view(),name='student-dashboard'),
    path('student-profile/',StudentProfile.as_view(),name='student-profile'),
    path('student-update/<int:pk>/',StudentProfileUpdate.as_view(),name='student-update'),
]
