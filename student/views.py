from django.shortcuts import render
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView



from .models import Student
from .forms import StudentUpdateForm


class StudentDashboard(TemplateView):

    def get (self,request):
        context = {}
        return render(request, 'session/dashboard/student-dashboard.html',{})

class StudentProfile(TemplateView):
    def get (self,request):
    	if('user_email' in request.session):
    		email = request.session['user_email']
    		id = request.session['user_id']
    		print('id',id)
    		student = Student.objects.get(id=id)
    		context = {
    		'student': student,
    		}
    		return render(request, 'student/student-profile.html',context)

class StudentProfileUpdate(UpdateView):
    template_name = 'student/student-update.html'
    model = Student
    form_class = StudentUpdateForm
    success_url = '/student-profile'
