from django import forms
from student.models import Student

class StudentUpdateForm(forms.ModelForm):
	sname = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': ''}
		), required=True, max_length=100)
	saddress = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': ''}
		), required=False, max_length=50)
	scontact = forms.IntegerField(widget=forms.NumberInput(
		attrs={'class': 'form-control', 'placeholder': ''}
		), required=False)
	semail = forms.CharField(widget=forms.EmailInput(
		attrs={'class': 'form-control', 'placeholder': ''}
		), required=False, max_length=50)
	sbatch = forms.IntegerField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': ''}
		), required=False)
	class Meta:
		model = Student
		fields = ['sname','saddress','scontact','semail','sfaculty','sbatch']