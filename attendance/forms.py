from django import forms
from .models import Daily

class DailyForm(forms.ModelForm):
    # std_name = forms.CharField(widget=forms.TextInput(
    #     attrs={'class': 'form-control', 'placeholder': 'Enter Name of Student'}
    # ), required=True, max_length=100)
    # date = forms.DateField(widget=forms.DateInput(
    #     attrs={'class': 'form-control', 'placeholder': 'Enter date ', 'id': 'Anniversary'}
    # ), required=False)
    # location = forms.CharField(widget=forms.TextInput(
    #     attrs={'class': 'form-control', 'placeholder': 'Enter  location'}
    # ), required=False, max_length=50)
    faculty = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter Faculty of Student'}
    ), required=True, max_length=100)
    batch = forms.IntegerField(widget=forms.NumberInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter batch'}
    ), required=False)


    class Meta:
        model = Organization
        fields = ['faculty', 'batch']
