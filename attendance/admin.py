from django.contrib import admin

from .models import Daily,Semester

admin.site.register(Daily)
admin.site.register(Semester)