from django.db import models

from student.models import Student

class Daily(models.Model):
	std_id = models.IntegerField(null=True,blank=True)
	std_name = models.CharField(max_length=100)
	date= models.DateField()
	attendance = models.BooleanField(default=False)
	faculty = models.CharField(max_length=20)
	batch = models.IntegerField()

	def __str__(self):
		return (self.std_name + " - Batch : " + str(self.batch))

class Semester(models.Model):
	# std_id = models.IntegerField()
	# roll_no= models.IntegerField()
	std_name = models.CharField(max_length=100)
	semester =  models.CharField(max_length=10)
	present_days = models.IntegerField(default=30)
	absent_days = models.IntegerField()
	working_days = models.IntegerField()
	faculty = models.CharField(max_length=20)
	batch = models.IntegerField()

class Meta:
		db_table = "semester"