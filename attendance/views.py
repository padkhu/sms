from django.shortcuts import render,redirect,reverse
from django.views.generic import TemplateView
import datetime
from django.http import HttpResponseRedirect, JsonResponse
from django.contrib import messages
from faculty.models import Faculty
from student.models import Student
from .models import Daily
from .models import Semester

from django.db.models import Count



class Attendance(TemplateView):
    def get (self,request):
        faculty=Faculty.objects.all()
        print('faculty',faculty)
        context = {
        'faculty':faculty,
        }
        return render(request, 'attendance/attendance.html',context)

class TakeAttendance(TemplateView):
	def get(self,request):
		return redirect(reverse('attendance:attendance'))
	def post(self,request):
		faculty = request.POST.get('faculty')
		print('faculty',faculty)
		batch = request.POST.get('batch')
		fac = Faculty.objects.get(fname=faculty)
		student = Student.objects.filter(sfaculty=fac.id,sbatch=batch)
		print('student',student)
		context = {
		'students':student,
		'faculty':fac,
		'batch' : batch,
		}
		return render(request,"attendance/take-attendance.html",context)


class Update_Attendance(TemplateView):
	def post(self, request, *args, **kwargs,):
		values = []
		names = []
		atts = []
		x = datetime.datetime.now()
		if request.method == "POST":
			count = int(request.POST.get('count'))
			
			for i in range(1, count+1):
				values.append(request.POST.get('id{}'.format(i)))
				print('values-format',values)
				names.append(request.POST.get('name{}'.format(i)))
				print('name-format',names)
				atts.append(request.POST.get('att{}'.format(i)))
				print('atten',atts)
				id=values[i-1]
				name=names[i-1]
				atte = atts[i-1]
				print('id-',id)
				print('name-',name)
				print('atten-',atte)
				print('faculty',self.kwargs['faculty'])
				print('batch',self.kwargs['batch'])
				att=Daily(std_id=id,std_name=name,date=x.strftime("%Y-%m-%d"),attendance=atte,faculty=self.kwargs['faculty'],batch=self.kwargs['batch'])
				att.save()
			
			messages.success(request,'Attendance successfully updated')
			return HttpResponseRedirect('/attendance')


class ViewAttendance(TemplateView):
	def get(self,request):
		return redirect(reverse('attendance:attendance'))
	def post(self,request):
		faculty = request.POST.get('faculty')
		batch = request.POST.get('batch')
		sem = request.POST.get('semester')
		distinct = Daily.objects.values('std_name').annotate(name_count=Count('std_name'))
		# print('-----------',distinct)
		att1 = ([item['std_name'] for item in distinct])
		# print('attribute',att1)
		# att = Daily.objects.filter(faculty=faculty, batch=batch,std_name__in=[item['std_name'] for item in distinct])
		
		for attribute in att1:
			attendances=Daily.objects.filter(faculty=faculty, batch=batch,std_name=attribute)
			present_days=0
			absent_days=0
			working_days=0
			# print('attendances',attendances)
			for attendance_attribute in attendances:
				if attendance_attribute.attendance == True:
					present_days=present_days+1
				elif attendance_attribute.attendance==False:
					absent_days = absent_days+1
				working_days = present_days+absent_days
			calls=Semester.objects.all()
			# print('calls------------',calls)
			semester=Semester(std_name=attendance_attribute.std_name,semester=sem,present_days=present_days,absent_days=absent_days,working_days=working_days,faculty=faculty,batch=batch)
			print('sem',semester.std_name)
			print('sem-id',semester.id)
			distinct1 = Semester.objects.values('std_name').annotate(name_count=Count('std_name'))
			att2 = ([item1['std_name'] for item1 in distinct1])
			if calls:
				print('insideif------------------')
				for call in att2:
					if semester.std_name in att2:
						print('update')
						p=Semester.objects.get(std_name=semester.std_name)
						print('p',p)
						c=Semester.objects.filter(id=p.id).update(std_name=attendance_attribute.std_name,semester=sem,present_days=present_days,absent_days=absent_days,working_days=working_days,faculty=faculty,batch=batch)
						print('update-working-days',c)
					else:
						semester.save()
			else:
				semester.save()
		# for attendances in att:
		# 	if attendances.attendance==True:
		# 		present_days=present_days+1
		# 	elif attendances.attendance==False:
		# 		absent_days = absent_days+1
			# print('present_days',present_days)
			# print('absent_days',absent_days)
			# print('working_days',working_days)
		student = Semester.objects.filter(semester=sem,batch=batch,faculty=faculty)
		context = {
		'students':student,

		}
		return render(request,"attendance/view-attendance.html",context)


# class StdAttendance(TemplateView):
# 	def get(self,request):
# 		return render(request,"attendance/piechart.html",{})
# 	def post(self,request):
# 		name = request.session['user_name']
# 		print('name',name)
# 		if request.method == "POST":
# 			sem=request.POST.get('sem')
# 			attendance=Semester.objects.get(std_name=name,semester=sem)
# 			context = {
# 			'sem':sem,
# 			'attendance':attendance,
# 			}
# 			return render(request,"attendance/piechart.html",context)
# 		else:
# 			sem='First'
# 			attendance= Semester.objects.get(std_name=name,semester=sem)
# 			# attendance= semester.objects.filter(std_id=id)
# 			# return HttpResponse(attendance)
# 			context = {
# 			'sem':sem,
# 			'attendance':attendance,
# 			}
# 			return render(request,"attendance/piechart.html",context)


def StdAttendance(request):
	name = request.session['user_name']
	if request.method == "POST":
		sem=request.POST.get('sem')
		attendance=Semester.objects.get(std_name=name,semester=sem)
		context = {
		'sem':sem,
		'attendance':attendance,
		}
		return render(request,"attendance/piechart.html",context)
	else:
		sem='First'
		attendance= Semester.objects.get(std_name=name,semester=sem)
		# attendance= semester.objects.filter(std_id=id)
		# return HttpResponse(attendance)
		context = {
		'sem':sem,
		'attendance':attendance,
		}
		return render(request,"attendance/piechart.html",context)

