from django.urls import path
from .views import (
    Attendance,
    TakeAttendance,
    # update_attendance,
    Update_Attendance,
    ViewAttendance,
    StdAttendance,
	)

app_name='attendance'
urlpatterns = [
    path('attendance/',Attendance.as_view(),name='attendance'),
    path('take-attendance/',TakeAttendance.as_view(),name='take-attendance'),
    path('Update-Attendance/<str:faculty>/<int:batch>/',Update_Attendance.as_view(),name='update-attendance'),
    path('view-attendance',ViewAttendance.as_view(),name='view-attendance'),
    path('student-attendance',StdAttendance,name='student-attendance'),
]
