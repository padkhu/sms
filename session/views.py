from django.shortcuts import render,redirect
from django.views.generic.edit import FormView
from student.models import Student
from teacher.models import Teacher
from faculty.models import Faculty
from django.http import HttpResponse

from .forms import LoginForm

class LoginView(FormView):
    template_name = "session/login.html"

    def get(self, request, *args, **kwargs):
        form = LoginForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request, **kwargs):
        if request.method == "POST":
            email = request.POST.get('email')
            password = request.POST.get('password')
            teacher = Teacher.objects.filter(temail=email,tpassword=password)
            num_rows = teacher.count()
            if(num_rows>0):
                for t in teacher:
                    request.session['user_email'] = t.temail
                    request.session['user_name'] = t.tname
                    request.session['tid'] = t.id
                    request.session['user_facultyid'] = t.tfaculty.id
                    request.session['position'] = t.tposition
                return redirect('teacher:teacher-dashboard')
            else:
                student = Student.objects.filter(semail=email,spassword=password)
                num_rows = student.count()
                if(num_rows>0):
                    for s in student:
                        request.session['user_email'] = s.semail
                        request.session['user_name'] = s.sname
                        request.session['user_facultyid'] = s.sfaculty.id
                        fac_id = s.sfaculty_id
                        print('faculty_id',fac_id)
                        print('fname',s.sfaculty.id)
                        faculty = Faculty.objects.get(fid=fac_id)
                        request.session['user_faculty'] = faculty.fname
                        request.session['user_batch'] = s.sbatch
                        request.session['user_id']=s.id
                    # request.session['user_email'] = email
                    # ema = request.session['user_faculty']
                    return redirect('student:student-profile')
                else:
                    return HttpResponse('Email or password incorrect')  
        else:
            return HttpResponse("Please Retry after some time")
        return HttpResponse("not POST")


def logout(request):
    request.session.flush()
    return redirect('session:login')

# from django.contrib.auth.decorators import login_required

# @login_required(login_url='/accounts/login/')
# def my_view(request):