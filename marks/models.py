from django.db import models

from student.models import Student

class Marks(models.Model):
    sname = models.CharField(max_length=30)
    msemester = models.CharField(max_length=50)
    mexam = models.CharField(max_length=50)
    msubject = models.CharField(max_length=50)
    mmarks = models.IntegerField()
    mbatch = models.IntegerField(default=2072)
    fname = models.CharField(max_length=50,default='BSCCSIT')
    student = models.ForeignKey(Student,on_delete = models.SET_NULL,null = True,blank = True,related_name = 'student_marks')

    class Meta:
        db_table = "marks"

    def __str__(self):
        return self.msemester
