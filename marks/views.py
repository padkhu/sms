from django.shortcuts import render,redirect,reverse
from django.views.generic import TemplateView
from django.contrib import messages
from django.http import HttpResponse
from django.http import HttpResponseRedirect,JsonResponse
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
import sys,json

from faculty.models import Faculty
from student.models import Student
from .models import Marks
from .forms import MarksForm,MarksCreateForm


class MarksInp(TemplateView):
    def get (self,request):
        faculty=Faculty.objects.all()
        print('faculty',faculty)
        context = {
        'faculty':faculty,
        }
        return render(request, 'marks/marks-input.html',context)

class ProvideMarks(TemplateView):
	def post(self,request):
		afname = request.POST.get('faculty')
		asbatch = request.POST.get('batch')
		semester = request.POST.get('semester')
		exam = request.POST.get('exam')
		subject = request.POST.get('subject')
		faculty = Faculty.objects.get(fname=afname)
		faculty_id = faculty.id
		student = Student.objects.filter(sfaculty_id = faculty_id , sbatch = asbatch)
		context = {
		'faculty':afname,
		'batch' : asbatch,
		'semester' : semester,
		'exam' : exam,
		'subject' : subject,
		'students' : student,
		}
		marks = Marks.objects.filter(fname=afname,msubject=subject,msemester=semester,mbatch=asbatch,mexam=exam)
		if marks:
			messages.success(request,'Marks already uploaded')
			return render(request,"marks/marks-input.html",{})
		else:
			return render(request, "marks/provide-marks.html", context)


class Add_Marks(TemplateView):
	def post(self,request):
		mid=[]
		mexam = []
		mname = []
		msub=[]
		mmarks = []
		msem = []
		mbat = []
		mfna = []

		if request.method == "POST":
			count = int(request.POST.get('count'))
			print('count',count)
			for i in range(1, count+1):
				mid.append(request.POST.get('id{}'.format(i)))
				mexam.append(request.POST.get('exam{}'.format(i)))
				mname.append(request.POST.get('name{}'.format(i)))
				msub.append(request.POST.get('sub{}'.format(i)))
				mmarks.append(request.POST.get('marks{}'.format(i)))
				msem.append(request.POST.get('sem{}'.format(i)))
				mbat.append(request.POST.get('bat{}'.format(i)))
				mfna.append(request.POST.get('fna{}'.format(i)))
				id=mid[i-1]
				exam=mexam[i-1]
				name=mname[i-1]
				sub=msub[i-1]
				marks = mmarks[i-1]
				semester=msem[i-1]
				batch=mbat[i-1]
				fac=mfna[i-1]
				mar=Marks(msemester=semester,mexam=exam,msubject=sub,mmarks=marks,fname=fac,mbatch=batch,sname=name)
				mar.save()
			messages.success(request,'Marks successfully uploaded')
			# return redirect("../../marks")
			return HttpResponseRedirect('/marks-input')
		return HttpResponse('else')


# ajax

def get_marks(request):
    data = {'success': False} 
    if request.method=='POST':
        marks = request.POST.get('userbooks')
        mark = json.dumps(marks)
        print('marks',marks)
        print('mark',mark)
        data['success'] = True
        context = {
        'hey_marks' :marks,
        'hey_mark' : mark,
        }

    return render(request,"marks/show-marks.html",context)
# ajax

def get_package_detail(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        data1 = data['array']
        # data4 = data.dumps(data)
        print('data1',data1)
        context = {
        'data1': data1,
        # 'data4':data4,
        }
        return JsonResponse(context)
    else:
    	return render(request,"marks/show-marks.html",{})



class ShowMarks(TemplateView):
    def get(self,request):
        marks = Marks.objects.all()
        print('marks',marks)
        for mark in marks:
            print(mark.sname)
        return redirect(reverse('marks:marks-input'))
    def post(self,request):
        fname = request.POST.get('faculty')
        mbatch = request.POST.get('batch')
        msemester = request.POST.get('semester')
        marks = Marks.objects.filter(msemester=msemester,mbatch=mbatch,fname=fname)
        return render(request,"marks/show-marks.html",{'marks' : marks })



class MarksUpdate(UpdateView):
    model = Marks
    template_name = 'marks/marks-update.html'
    form_class = MarksForm
    success_url = '/show-marks'

def deletemarks(request, id):
    marks = Marks.objects.get(id=id)
    marks.delete()
    return redirect(reverse('marks:marks-input'))

class MarksCreate(CreateView):
    model=Marks
    template_name = 'marks/marks-create.html'
    form_class = MarksCreateForm
    success_url = '/marks-input/'


class MarksOfStudent(TemplateView):
    def get(self,request):
        faculty=request.session['user_faculty']
        batch=request.session['user_batch']
        sid=request.session['user_id']
        sname=request.session['user_name']
        totalt=0
        totalm=0
        totalp=0
        scount_test=0
        scount_mid=0
        scount_pre=0
        flag1='Pass'
        flag2='Pass'
        flag3='Pass'
        p=[]
        marks = Marks.objects.filter(sname=sname,mbatch=batch,fname=faculty)
        print('marks',marks)
        marks_test = Marks.objects.filter(sname=sname,mbatch=batch,fname=faculty,mexam='Class Test')
        print('class test',marks_test)
        marks_mid = Marks.objects.filter(sname=sname,mbatch=batch,fname=faculty,mexam='Mid-Term')
        marks_pre = Marks.objects.filter(sname=sname,mbatch=batch,fname=faculty,mexam='Pre-Board')

        for mt in marks_test:
            totalt=totalt+mt.mmarks
            if (mt.msubject):
                scount_test=scount_test+1
            if(mt.mmarks<24):
                flag1='Fail'
        
        for mm in marks_mid:
            totalm = totalm + mm.mmarks
            if (mt.msubject):
                scount_mid=scount_mid+1
            if(mm.mmarks<24):
                flag2='Fail'

        for mp in marks_pre:
            totalp = totalp + mp.mmarks
            if (mt.msubject):
                scount_pre=scount_pre+1
            if(mp.mmarks<24):
                flag3='Fail'

        percent={}
        if scount_test!=0:
            percent['test_percent']=(totalt/scount_test)
        if scount_mid!=0:
            percent['mid_percent']=(totalm/scount_mid)
            print('percent',percent['mid_percent'])
        if scount_pre!=0:
            percent['pre_percent']=(totalp/scount_pre)
        percent['test_pass']=flag1
        percent['mid_pass'] = flag2
        percent['pre_pass'] = flag3
        

        # m1={'marks':marks}
        # m2={'marks_mid':marks_mid}
        # m3={'marks_pre':marks_pre}
        # m= {**m1,**m2,**m3,**percent}

        context = {
        'marks_test':marks_test,
        'marks_mid':marks_mid,
        'marks_pre':marks_pre,
        **percent,

        }
        return render(request,"marks/marks-student.html",context)

