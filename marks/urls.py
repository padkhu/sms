from django.urls import path
from .views import (
    MarksInp,
    ProvideMarks,
    Add_Marks,
    ShowMarks,
    MarksUpdate,
    get_package_detail,
    get_marks,
    deletemarks,
    MarksCreate,
    MarksOfStudent,
	)

app_name='marks'
urlpatterns = [
    path('marks-input/',MarksInp.as_view(),name='marks-input'),
    path('provide-marks/',ProvideMarks.as_view(),name='provide-marks'),
    path('add-marks',Add_Marks.as_view(),name='add-marks'),
    path('show-marks',ShowMarks.as_view(),name='show-marks'),
    path('marks-update/<int:pk>/',MarksUpdate.as_view(),name='marks-update'),
    path('marks-create/',MarksCreate.as_view(),name='marks-create'),
    path('delete-marks/<int:id>/',deletemarks,name='delete-marks'),
    path('marks-student/',MarksOfStudent.as_view(),name='marks-student'),

    path('package-detail/', get_package_detail, name='package-request'),
    path('get-marks/',get_marks,name='marks-request'),
    

]
