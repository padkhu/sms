from django import forms
from marks.models import Marks


class MarksForm(forms.ModelForm):
	sname = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter Name Of Student'}
		), required=True, max_length=100)
	msemester = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter Contact Person Name'}
		), required=False, max_length=50)
	mexam = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter  location'}
		), required=False, max_length=50)
	msubject = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter  location'}
		), required=False, max_length=50)
	mmarks = forms.IntegerField(widget=forms.NumberInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter Contact Number'}
		), required=False)
	class Meta:
		model = Marks
		fields = ['sname','msemester','mexam','msubject','mmarks']

class MarksCreateForm(forms.ModelForm):
	sname = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter Name Of Student'}
		), required=True, max_length=100)
	msemester = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter Contact Person Name'}
		), required=False, max_length=50)
	mexam = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter  location'}
		), required=False, max_length=50)
	msubject = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter  location'}
		), required=False, max_length=50)
	mmarks = forms.IntegerField(widget=forms.NumberInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter Contact Number'}
		), required=False)
	mbatch = forms.IntegerField(widget=forms.NumberInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter Contact Number'}
		), required=False)
	fname = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter  location'}
		), required=False, max_length=50)
	class Meta:
		model = Marks
		fields = ['sname','msemester','mexam','msubject','mmarks','mbatch','fname']
