from django.shortcuts import render,redirect
from django.views.generic import TemplateView
from .models import Teacher
from .forms import TeacherUpdateForm
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.http import HttpResponseRedirect, JsonResponse

from notice.models import Notices
from routine.models import Routine



class testing(TemplateView):

    def get (self,request):
        context = {}
        return render(request, 'testing.html',{})

class dashboard(TemplateView):
    def get (self,request):
        notices = Notices.objects.all()
        routines = Routine.objects.all()
        context = {
        'notices':notices,
        'routines':routines,
        }
        return render(request, 'session/dashboard/teacher-dashboard.html',context)

class TeacherProfile(TemplateView):
    def get (self,request):
    	if('user_email' in request.session):
    		email = request.session['user_email']
    		id = request.session['tid']
    		print('id',id)
    		teacher = Teacher.objects.get(id=id)
    		context = {
    		'teacher': teacher,
    		}
    		return render(request, 'teacher/teacher-profile.html',context)

class TeacherUpdate(UpdateView):
    template_name = 'teacher/teacher-update.html'
    model = Teacher
    form_class = TeacherUpdateForm
    success_url = '/teacher-profile'

    def get_initial(self):
        initial = super(TeacherUpdate, self).get_initial()
        print('initial data', initial)
        habit_object = self.get_object()

        # initial['field1'] = habit_object.field1
        # initial['field2'] = habit_object.field2
        return initial

# class TeacherUpdate(UpdateView):

#     template_name = 'teacher/teacher-update.html'
#     form_class = TeacherUpdateForm

#     def get(self, request, *args, **kwargs):
#         teacher = Teacher.objects.get(id=self.kwargs['pk'])
#         teacherUpdateform = TeacherUpdateForm(instance=teacher)
#         return render(request, self.template_name, {'form': teacherUpdateform, 'teacher': teacher})

#     def post(self, request, *args, **kwargs):
#         form = TeacherUpdateForm(request.POST, request.FILES or None)
#         print('name',form['tname'].value())
#         print('address',form['taddress'].value())
#         print('tcontact',form['tcontact'].value())
#         print('email',form['temail'].value())
#         print('faculty',form['tfaculty'].value())
#         print('position',form['tposition'].value())
    
#         teacher = Teacher.objects.get(id=self.kwargs['pk'])
#         Teacher.objects.filter(id=teacher.id).update(tname=form['tname'].value(),
#                                                          taddress=form['taddress'].value(),
#                                                          tcontact=form['tcontact'].value(),
#                                                          temail=form['temail'].value(),
#                                                          tpassword=teacher.tpassword,
#                                                          tfaculty=form['tfaculty'].value(),
#                                                          tposition=form['tposition'].value())
#         return redirect('teacher:teacher-profile')
