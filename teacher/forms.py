from django import forms
from teacher.models import Teacher

class TeacherUpdateForm(forms.ModelForm):
	tname = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': ''}
		), required=True, max_length=100)
	taddress = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': ''}
		), required=False, max_length=50)
	tcontact = forms.IntegerField(widget=forms.NumberInput(
		attrs={'class': 'form-control', 'placeholder': ''}
		), required=False)
	temail = forms.CharField(widget=forms.EmailInput(
		attrs={'class': 'form-control', 'placeholder': ''}
		), required=False, max_length=50)
	tposition = forms.IntegerField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': ''}
		), required=False)
	class Meta:
		model = Teacher
		fields = ['tname','taddress','tcontact','temail','tfaculty','tposition']