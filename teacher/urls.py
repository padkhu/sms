from django.urls import path
from .views import (
    testing,
    dashboard,
    TeacherProfile,
    TeacherUpdate,
	)

app_name='teacher'
urlpatterns = [
    path('testing',testing.as_view(),name='testing'),
    path('teacher-dashboard/',dashboard.as_view(),name='teacher-dashboard'),
    path('teacher-profile/',TeacherProfile.as_view(),name='teacher-profile'),
    path('teacher-update/<int:pk>/',TeacherUpdate.as_view(),name='teacher-update'),

]
