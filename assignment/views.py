from django.shortcuts import render,redirect,reverse
from django.views.generic import ListView
from django.views.generic import TemplateView


from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.edit import FormView

from .models import Assignment
from .forms import AssignmentCreateForm,AssignmentUpdateForm
from faculty.models import Faculty

class AssignmentList(ListView):

    template_name = 'assignment/assignment-list.html'

    def get(self, request, *args, **kwargs):
        assignment = Assignment.objects.all()
        return render(request, self.template_name, {'assignments':assignment})

class AssignmentCreate(CreateView):
    model=Assignment
    template_name = 'assignment/assignment-create.html'
    form_class = AssignmentCreateForm
    success_url = '/assignment-list/'

    def get_context_data(self, **kwargs):
        context = super(AssignmentCreate, self).get_context_data(**kwargs)
        # context['user'] = self.get_object()
        obj = Faculty.objects.all()
        context['faculty'] = obj
        return context

# class AssignmentCreate(FormView):
    
#     def get(self, request):
#         content={}
#         content['form'] =AssignmentCreateForm
#         return render(request, 'assignment/assignment-create.html', content)

#     def post(self, request):
#         content = {}
#         form = AssignmentCreateForm(request.POST, request.FILES or None)
#         adeadline = request.POST.get('adeadline',None)
#         # print('deadline',adeadline)
#         asbatch= request.POST.get('asbatch',None)
#         if form.is_valid():
#             print('document',form.cleaned_data['document'])
#             # print('adeadline',adeadline)
            
#             Assignment.objects.create(document=form.cleaned_data['document'],adeadline=adeadline,afname=form.cleaned_data['afname'],asbatch=asbatch,asubject=form.cleaned_data['asubject'],aname=form.cleaned_data['aname'])
#             return redirect(reverse('assignment:assignment-list'))
#         content['form'] = form
#         return render(request, 'assignment/assignment-create.html', content)

class AssignmentUpdate(UpdateView):
    model=Assignment
    template_name = 'assignment/assignment-update.html'
    form_class = AssignmentUpdateForm
    success_url = '/assignment-list/'

class AssignmentDelete(DeleteView):
    template_name = 'assignment/assignment-delete.html'
    model = Assignment
    success_url = '/assignment-list/'


class StudentAssignmentList(TemplateView):

    template_name = 'assignment/student-assignment-list.html'

    def get(self, request, *args, **kwargs):
        assignment = Assignment.objects.all()
        return render(request, self.template_name, {'assignments':assignment})

