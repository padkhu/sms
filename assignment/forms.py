from django import forms
from .models import Assignment

class AssignmentCreateForm(forms.ModelForm):
	afname = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter faculty Here'}
		))
	asbatch = forms.IntegerField(widget=forms.NumberInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter Batch Here'}
		))
	aname = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter Assignment Name Here'}
		))
	asubject = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter  Subject Name Here'}
		))
	adeadline = forms.DateField(widget=forms.DateInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter Deadline Here'}
    ))
	document = forms.FileField(widget=forms.ClearableFileInput(
		attrs={'class':'form-control','placeholder':'File Here'}))
	class Meta:
		model = Assignment
		fields = ['afname','asbatch','aname','asubject','adeadline','document']


class AssignmentUpdateForm(forms.ModelForm):
	afname = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter faculty Here'}
		))
	asbatch = forms.IntegerField(widget=forms.NumberInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter Batch Here'}
		))
	aname = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter Assignment Name Here'}
		))
	asubject = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter  Subject Name Here'}
		))
	adeadline = forms.DateField(widget=forms.DateInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter Deadline Here','id':'adeadline'}
    ))
	document = forms.FileField(widget=forms.ClearableFileInput(
		attrs={'class':'form-control','placeholder':'File Here'}))
	class Meta:
		model = Assignment
		fields = ['afname','asbatch','aname','asubject','adeadline','document']
