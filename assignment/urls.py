from django.urls import path
from .views import (
	AssignmentList,
    AssignmentCreate,
    AssignmentUpdate,
    AssignmentDelete,
    StudentAssignmentList,
	)

app_name='assignment'
urlpatterns = [
	path('assignment-list/',AssignmentList.as_view(),name='assignment-list'),
    path('assignment-create/',AssignmentCreate.as_view(),name='assignment-create'),
    path('assignment-update/<int:pk>/',AssignmentUpdate.as_view(),name='assignment-update'),
    path('assignment-delete/<int:pk>/',AssignmentDelete.as_view(),name='assignment-delete'),
    path('student-assignment-list/',StudentAssignmentList.as_view(),name='student-assignment'),
]
