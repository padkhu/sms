from django.db import models


class Assignment(models.Model):
    afname = models.CharField(max_length=100)
    asbatch = models.IntegerField()
    aname = models.CharField(max_length=200)
    asubject = models.CharField(max_length=200)
    adeadline = models.DateField(auto_now=False)
    document = models.FileField(default = None, blank=True)

    class Meta:
        db_table = "assignment"
    def __str__(self):
    	return (self.aname + " - Batch : " + str(self.abatch))
