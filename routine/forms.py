from django import forms
from .models import Routine

class RoutineCreateForm(forms.ModelForm):
	routine = forms.FileField(widget=forms.ClearableFileInput(
		attrs={'class':'form-control','placeholder':'File Here'}))
	uploaded_at = forms.DateField(widget=forms.DateInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter Date Here'}
    ))
	rfaculty = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter faculty Here'}
		))
	rbatch = forms.IntegerField(widget=forms.NumberInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter Batch Here'}
		))
	rsemester = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter rsemester Here'}
		))
	rtitle = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter title Here'}
		))
	class Meta:
		model = Routine
		fields = ['rfaculty','rbatch','rsemester','uploaded_at','rtitle','routine']