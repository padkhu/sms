from django.urls import path
from .views import (
    RoutineCreate,
	)

app_name='routine'
urlpatterns = [
    path('routine-create/',RoutineCreate.as_view(),name='routine-create'),
]
