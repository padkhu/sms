from django.shortcuts import render,redirect,reverse
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.edit import FormView

from .models import Routine
from .forms import RoutineCreateForm


class RoutineCreate(CreateView):
    model=Routine
    template_name = 'routine/routine-create.html'
    form_class = RoutineCreateForm
    success_url = '/routine-create/'

    def get_context_data(self, **kwargs):
        context = super(RoutineCreate, self).get_context_data(**kwargs)
        # context['user'] = self.get_object()
        obj = Routine.objects.all()
        context['routines'] = obj
        return context

# class RoutineCreate(FormView):
    
#     def get(self, request):
#         content={}
#         content['form'] =RoutineCreateForm
#         return render(request, 'routine/routine-create.html', content)

#     def post(self, request):
#         content = {}
#         form = RoutineCreateForm(request.POST, request.FILES or None)
#         uploaded_at = request.POST.get('uploaded_at',None)
#         print('display_upto',display_upto)
#         if form.is_valid():
#             Routine.objects.create(routine=form.cleaned_data['routine'],uploaded_at=uploaded_at,rfaculty=form.cleaned_data['rfaculty'],rbatch=form.cleaned_data['rbatch'],rsemester=form.cleaned_data['rsemester'],rtitle=form.cleaned_data['rtitle'])
#             return redirect(reverse('routine:routine-create'))
#         content['form'] = form
#         return render(request, 'routine/routine-create.html', content)

#     