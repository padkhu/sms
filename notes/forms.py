from django import forms
from .models import Notes

FILE_FORMAT_CHOICES = [("dxf","Download DXF"),("txt", "Download TXT"),("csv", "Download CSV"),("pdf","Download PDF")]

class FileFormatForm(forms.Form):
	file_format = forms.ChoiceField(choices=FILE_FORMAT_CHOICES,widget=forms.RadioSelect())

# class FileFormatForm(forms.Form):
#     file_format = forms.ChoiceField(choices=FILE_FORMAT_CHOICES, widget=forms.RadioSelect())

class NoteCreateForm(forms.ModelForm):
	ndocument = forms.FileField(widget=forms.ClearableFileInput(
		attrs={'class':'form-control','placeholder':'File Here'}))
	uploaded_at = forms.DateField(widget=forms.DateInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter Date Here'}
    ))
	nfaculty = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter faculty Here'}
		))
	nbatch = forms.IntegerField(widget=forms.NumberInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter Batch Here'}
		))
	nname = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter Position Here'}
		))
	class Meta:
		model = Notes
		fields = ['ndocument','uploaded_at','nfaculty','nbatch','nname']
 

class NoteUpdateForm(forms.ModelForm):
	ndocument = forms.FileField(widget=forms.ClearableFileInput(
		attrs={'class':'form-control','placeholder':'File Here'}))
	uploaded_at = forms.DateField(widget=forms.DateInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter Date Here','id':'uploaded_at'}
    ))
	nfaculty = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter faculty Here'}
		))
	nbatch = forms.IntegerField(widget=forms.NumberInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter Batch Here'}
		))
	nname = forms.CharField(widget=forms.TextInput(
		attrs={'class': 'form-control', 'placeholder': 'Enter Position Here'}
		))
	class Meta:
		model = Notes
		fields = ['ndocument','uploaded_at','nfaculty','nbatch','nname']



