from django.shortcuts import render,redirect,reverse
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic import TemplateView

from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.edit import FormView


from .models import Notes
from django.http import FileResponse
from .forms import NoteCreateForm,NoteUpdateForm,FileFormatForm
# from .forms FileFormatForm


# class NoteList(ListView):
#     model = Notes
#     template_name = 'notes/note-list.html'
#     form_class = FileFormatForm
#     context_object_name = 'notes'

class NoteList(ListView):

    template_name = 'notes/note-list.html'

    def get(self, request, *args, **kwargs):
        notes = Notes.objects.all()
        noteform = FileFormatForm()
        return render(request, self.template_name, {'form': noteform,'notes':notes})

class NoteDownload(DetailView):

    template_name = 'notes/note-download.html'

    def get(self, request, *args, **kwargs):
        note = Notes.objects.get(id=self.kwargs['pk'])
        noteform = FileFormatForm()
        return render(request, self.template_name, {'form': noteform,'note':note})



class NoteListDetail(DetailView):
    model=Notes
    def post(self, request, *args, **kwargs):
        assert request.method == 'POST', "users should only come here with POST now"
        note = Notes.objects.get(id=self.kwargs['pk'])
        form = FileFormatForm(request.POST)  # populate from POST
        if form.data['file_format'] == 'txt':
            response = FileResponse(open('{{note.ndocument.url}}', 'rb'))
            response['Content-Disposition'] = "attachment; filename='file.txt'"
            return response
        else:
            return "return CSV file response here"

class NoteCreate(CreateView):
    model=Notes
    template_name = 'notes/note-create.html'
    form_class = NoteCreateForm
    success_url = '/note-list/'

    def form_valid(self,form):
        print('form',form.cleaned_data)
        return super().form_valid(form)

# class NoteCreate(FormView):
    
#     def get(self, request):
#         content={}
#         content['form'] =NoteCreateForm
#         return render(request, 'notes/note-create.html', content)

#     def post(self, request):
#         content = {}
#         form = NoteCreateForm(request.POST, request.FILES or None)
#         uploaded_at = request.POST.get('uploaded_at',None)
#         if form.is_valid():
#             Notes.objects.create(ndocument=form.cleaned_data['ndocument'],uploaded_at=uploaded_at,nfaculty=form.cleaned_data['nfaculty'],nbatch=form.cleaned_data['nbatch'],nname=form.cleaned_data['nname'])
#             return redirect(reverse('notes:note-list'))
#         content['form'] = form
#         return render(request, 'notes/note-create.html', content)

class NoteUpdate(UpdateView):
    model=Notes
    template_name = 'notes/note-update.html'
    form_class = NoteUpdateForm
    success_url = '/note-list/'

# class NoteUpdate(UpdateView):

#     template_name = 'notes/note-update.html'

#     def get(self, request, *args, **kwargs):
#         note = Notes.objects.get(id=self.kwargs['pk'])
#         noteform = NoteCreateForm(instance=note)
#         return render(request, self.template_name, {'form': noteform})

class NoteDelete(DeleteView):
    template_name = 'notes/note-delete.html'
    model = Notes
    success_url = '/note-list/'

class StudentNotes(TemplateView):
    template_name = 'notes/note-student-list.html'
    def get(self, request, *args, **kwargs):
        notes = Notes.objects.all()
        return render(request, self.template_name, {'notes':notes})

