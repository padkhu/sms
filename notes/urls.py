from django.urls import path

from .views import (
    NoteList,
    NoteDownload,
    NoteListDetail,
    NoteCreate,
    NoteUpdate,
    NoteDelete,
    StudentNotes,
	)

app_name='notes'
urlpatterns = [
    path('note-list/',NoteList.as_view(),name='note-list'),
    path('note-download/<int:pk>/',NoteDownload.as_view(),name='note-download'),
    path('note-detail/<int:pk>/',NoteListDetail.as_view(),name='note-detail'),
    path('note-create/',NoteCreate.as_view(),name='note-create'),
    path('note-update/<int:pk>/',NoteUpdate.as_view(),name='note-update'),
    path('note-delete/<int:pk>/',NoteDelete.as_view(),name='note-delete'),
    path('student-note/',StudentNotes.as_view(),name='student-note'),
]
