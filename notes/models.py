from django.db import models

class Notes(models.Model):
	ndocument = models.FileField(blank=True,null=True)
	uploaded_at = models.DateField(auto_now=False,blank=True,null=True)
	nfaculty = models.CharField(max_length=200)
	nbatch = models.IntegerField()
	nname = models.CharField(max_length=300)
	class Meta:
		db_table ='notes'
	def __str__(self):
		return self.nname


		